/*
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 *
 *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *            佛祖保佑     永不宕机     永无BUG
 *
 *        佛曰:
 *                写字楼里写字间，写字间里程序员；
 *                程序人员写程序，又拿程序换酒钱。
 *                酒醒只在网上坐，酒醉还来网下眠；
 *                酒醉酒醒日复日，网上网下年复年。
 *                但愿老死电脑间，不愿鞠躬老板前；
 *                奔驰宝马贵者趣，公交自行程序员。
 *                别人笑我忒疯癫，我笑自己命太贱；
 *                不见满街漂亮妹，哪个归得程序员？
 */

/*
 * @Author: luox 9117143+whriten@user.noreply.gitee.com
 * @Date: 2023-11-29 10:19:23
 * @LastEditors: luox 9117143+whriten@user.noreply.gitee.com
 * @LastEditTime: 2023-12-16 14:23:18
 * @FilePath: \blog-server\app.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */



const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const Router = require('./src/router/index')
const cors = require('cors')
const path = require('path')
const OS = require('os')
const FileStreamRotator = require('file-stream-rotator')  // 引入日志分割组件
const fs = require('fs')
const morgan = require('morgan')

// let accessLogStream = fs.createWriteStream(path.join(__dirname, 'log/app.log'), {flags: 'a'}); // 日志配置

let logDirectory = path.join(__dirname, 'log')

// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

// create a rotating write stream
let accessLogStream = FileStreamRotator.getStream({
    date_format: 'YYYY-MM-DD',
    filename: path.join(logDirectory, 'app-%DATE%.log'),
    frequency: 'daily',
    verbose: false,
    max_logs:10, // 最长保留天数
})


console.log(OS.cpus()[0].model)
console.log('计算机名称',OS.hostname(),'操作系统',OS.type())
console.log('系统控件的内存',OS.freemem())
console.log('系统总内存',OS.totalmem())

app.use(bodyParser.urlencoded({extended:false,limit:'50mb'})) // 解析url编码的表单正文解析器
app.use(cors())
app.use(morgan('combined',)); // 引入日志 记录
require('./src/utils/randomNickName')
const {getClientIp, getClientIpCity} = require("./src/utils/util");

// 查看每个请求携带的参数
function logUse(req,res,next){
    // req.body ? console.log('body参数',req.body) : req.query ? console.log('query参数',req.query) : ''
    // console.log('method',req.method,'  URL',req.originalUrl)
    // console.log('访问者的ip____',getClientIp(req)) // 112.24.112.174 江苏省 盐城市
    // getClientIpCity(getClientIp(req))
    next()
}
app.use('/upload',express.static('upload'))
app.use('/api',logUse,Router)


app.listen(6066,() => {
    console.log('博客系统后台已启动 ： http://localhost:6066')
})
