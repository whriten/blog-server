const JWT = require('jsonwebtoken')

const secret = 'luox'

exports.generToken = (uid) => {
    const token = JWT.sign({
        uid
    }, secret, {
        expiresIn: 60*60*24*7 // 7天
    })
    return token
}

exports.verifyToken = (token) => {
    let result = JWT.verify(token, secret)
    if(result.uid){
        return result
    }
    return false
}
