const express = require('express')
const router = express.Router()
const {verifyToken} = require('../auth/token')




const {register,login,userList,deleteUser} = require('./user/index')

const {createArticle,deleteArticle,getArticle,getCurrentArticleContext,getHostArticleList,updateArticleViews,updateArticleLikes,updateArticleInfo,checkArticlePwd} = require('./article/index')
const {addCate,removeCate,updateCate,getCateArticle,getCateList} = require('./cate/index')
const { upload ,uploadHandler} = require('./upload/index')
const {getTagsList,addTag,deleteTag,getTagArticleList} = require('./tag/index')
const {addLinks,deleteLinks,queryLinks,updateLink}  = require('./links/index')
const { addLinkGroup,updateDynamicLinkGroup,deleteGroupLinks,getAllGroups } = require('./group/index') // 添加友链分组模块
const {uploadCateTagConfig,publicDate,publicConfig,getDashboardConfig,getTimeLineConfig,getMainFastConfig} = require('./globalConfig/index')

const db = require('../db/index')


// 解析token中的uid
let parseUid = (req,res,next) => {
    let {authorization} = req.headers
    if(authorization) {
       try {
           authorization = authorization.split(' ')[1]
           if(!verifyToken(authorization)) return res.send({code:400,message:'token过期'})
           let {uid} = verifyToken(authorization)
           req.uid = uid
           next()
       }catch (e) {
           res.send({code:401,message:e.message == 'jwt expired' ? 'token已经过期' : e.message})
           return;
       }
    }else {
        res.send({
            code: 400,
            message:'未登录状态禁止操作'
        })
        return
    }
}


// 中间件 去数据库查询当前用户(uid)是否是管理员如果不是则进行返回无权限

function checkUserType(req,res,next){
    let {uid} = req
    let selectUserSql = "select userType from users where userId = ?"
    db.query(selectUserSql,[uid],(err,result) => {
        if(err) return  res.send({code:400,message:err.sqlMessage})
        // console.log('check当前用户的信息',result[0])
        let {userType} = result[0]
        userType == 1 ? next() : res.send({code:400,message:'无权限操作'})
    })
}


// user模块
router.post('/register',register)
router.post('/login',login)
router.get('/user/delete/:userId',parseUid,checkUserType,deleteUser)
router.get('/user/list',parseUid,checkUserType,userList)

// 文章模块
router.post('/article/create',parseUid,createArticle) // 创建文章
router.delete('/article/delete/:articleId',parseUid,deleteArticle) // 根据id删除文章
router.get('/article/list',getCateArticle) // 查询当前分类下面所有文章
router.get('/article/hostList',getHostArticleList) // 查询热门文章
router.get('/article/context',getCurrentArticleContext) // 当前文章的上下文
router.post('/article/check',checkArticlePwd) // 锁定文章密码
router.get('/article/:articleId',getArticle) // 查看单个文章信息
router.put('/article/updateViews/:articleId',updateArticleViews) // 更新文章浏览量
router.put('/article/updateLikes/:articleId',updateArticleLikes) // 更新文章的点赞量
router.put('/article/update/:articleId',parseUid,checkUserType,updateArticleInfo) // 管理员更新文章对应的信息

// 分类模块
router.post('/cate/create',parseUid,checkUserType,addCate) // 添加分类
router.delete('/cate/delete',parseUid,checkUserType,removeCate) // 删除分类
router.put('/cate/update/:cateId',parseUid,checkUserType,updateCate) // 更新分类
router.get('/cate/list',getCateList) // 全部分类


// 标签模块
router.get('/tag/list',getTagsList)
router.delete('/tag/delete',parseUid,checkUserType,deleteTag)
router.post('/tag/create',parseUid,checkUserType,addTag)
router.post('/tag/articleList',getTagArticleList)
// 获取分类 标签列表
router.get('/upload/config',uploadCateTagConfig)

/* 友链模块 */
router.post('/link',parseUid,checkUserType,addLinks) // 添加友链
router.delete('/link',parseUid,checkUserType,deleteLinks) // 删除对应的友链
router.get('/link',queryLinks) // 获取所有友链信息
router.put('/link',parseUid,checkUserType,updateLink)// 更新友链
router.post('/upload/img',parseUid,upload,uploadHandler)

// 友链分组模块
router.get('/group',getAllGroups)
router.post('/group',parseUid,checkUserType,addLinkGroup) // 添加友链分组
router.put('/group/:groupId',parseUid,checkUserType,updateDynamicLinkGroup)
router.delete('/group/:groupId',parseUid,checkUserType,deleteGroupLinks) // 删除对应的友链分组

// 配置
router.get('/config/publicData',parseUid,checkUserType,publicDate)
router.get('/config/privateData',publicConfig)
router.get('/config/dashboard',getDashboardConfig)
router.get('/config/mainFast',getMainFastConfig)
// 时间轴 归档
router.get('/config/timeLine',getTimeLineConfig)

// 前台配置


router.get('/test',(req,res) => {
    res.send({
        code:200,
        message:'测试成功',
        data:{
            name:'test',
            id:Math.random()*100
        }
    })
})

module.exports = router

