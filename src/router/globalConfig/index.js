// 文章上传的配置接口 也就是获取文章分类和标签接口
const db = require("../../db");
var moment = require('moment')
exports.uploadCateTagConfig = (req, res) => {
    try {
        let findSql = "START TRANSACTION;" +
            "SELECT * FROM cates;" +
            "SELECT * from tags;" +
            "ROLLBACK;"
        db.query(findSql,(err,result) => {
            if(err) res.send({code:400,message:err.sqlMessage})
            let cates = result[1]
            let tags = result[2]
            res.send({code:200,message:'获取成功',cates,tags})
        })
    }catch (e) {
        res.send({code:400,message:e.message})
    }
}

/** 获取首页博客发布日期数据 */
exports.publicDate = (req,res) => {
    let selectSql = "SELECT articleId,articleTitle,publicTime from articles ORDER BY publicTime DESC"
    db.query(selectSql,(err,result) => {
        if(err) {
            console.log(err)
            return  res.send({code:400,message:err.sqlMessage})
        }
        result.forEach(item => {
            item["date"] = moment(item.publicTime).format('YYYY-MM-DD')
            item["count"] = 1
        })
        let arr = []
        let articles = []
        result.forEach(item => {
            if(arr.includes(item.date)) {
                articles.forEach(i => {
                    if(i.date == item.date) {
                        i.count++
                    }
                })
            }else {
                arr.push(item["date"])
                articles.push(item)
            }
        })
        res.send({code:200,message:'获取发布文章数据成功',articles,arr:result})
    })
}

/** 获取首页博客分类数据
 *  获取文章总量
 *  获取总浏览量
 *  获取阅读时长
 *  获取点赞量
 *  获取评论数量
 *  获取u最新发布文章的日期
 */
exports.publicConfig = (req,res) => {
    const selectSql = "select * from articles order by publicTime desc"
    db.query(selectSql,(err,result) => {
        if(err) {
            console.log(err)
            return  res.send({code:400,message:err.sqlMessage})
        }
        let count = result.length // 文章的总量
        let viewsCount = 0 // 浏览量
        let readTime = Math.floor(Math.random()*15) // 平均阅读时长
        let likeCount = 0
        let commentCount = 0 // 留言数量
        let updateTime = result[0].publicTime
        // console.log(result)
        result.forEach(item => {
            viewsCount += item.viewsCount*1
            likeCount += item.likeCount*1
            commentCount += item.repliesCount*1 // 留言量
        })
        let config = {
            count,
            viewsCount,
            readTime,
            likeCount,
            commentCount,
            updateTime:moment(updateTime).format('YYYY-MM-DD')
        }
        res.send({code:200,message:'获取首页博客分类数据成功',config})
    })
}

/**
 * @description 获取仪表盘配置
 *
 *
 *
 * */
exports.getDashboardConfig = (req,res) => {
    const selectSql = "select * from cates"
    db.query(selectSql,(err,result) => {
        if(err) {
            return  res.send({code:400,message:err.sqlMessage})
        }
        let dashboardConfig = {
            cateConfig:[], // 分类的比例配置

        }
        result.forEach(item => {
            let articlesArr = item["cateArticles"].split(',')
            let obj = {}
            obj.cateName = item["cateName"]
            obj.count = articlesArr.length
            obj.cateId = item["cateId"]
            dashboardConfig.cateConfig.push(obj)
        })
        res.send({code:200,message:'获取仪表盘配置成功',dashboardConfig})
    })
}


exports.getTimeLineConfig = (req,res) => {
    let sql = "SELECT DATE(publicTime) AS date, YEAR(publicTime) AS year, MONTH(publicTime) AS month, DAY(publicTime) AS day, articleId, articleTitle, COUNT(*) AS count FROM articles GROUP BY date, articleId ORDER BY year DESC, month DESC, day DESC"
    db.query(sql,(err,result) => {
        console.log('err',err);
        if(err) return res.send({code:400,message:err.sqlMessage})
        let MAP = new Map()
        result.forEach(item => {
            item.date = moment(item.date).format('YYYY-MM-DD hh:mm:ss')
            if(!MAP.has(item.year)){
                MAP.set(item.year,{count:0,list:[]})
            }
            MAP.get(item.year).count += item.count
            MAP.get(item.year).list.push(item)
        })
        console.log(MAP);
        let arrayMAP = Array.from(MAP)
        res.send({
            code:200,
            message:'获取时间线配置成功',
            timeLineConfig:result,
            mapConfig:arrayMAP
        })
    })
}

/*查询主页配置 文章分类数量*/
exports.getMainFastConfig = (req,res) => {
    const findSql = "select count(*) AS articleCount from articles;select count(*) tagCount from tags;select count(*) AS cateCount from cates"
    db.query(findSql,(err,result) => {
        if(err) return res.send({code:400,message:err.sqlMessage})
        let articleCount = result[0][0].articleCount
        let tagCount = result[1][0].tagCount
        let cateCount = result[2][0].cateCount
        res.send({
            code:200,
            message:'success',
            articleCount,
            tagCount,
            cateCount
        })
    })
}