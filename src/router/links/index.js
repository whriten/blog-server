const db = require('../../db/index');
const {verifyToken} = require('../../auth/token')






// 查询数据库，检查是否已存在相同的友链信息
async function checkExistingLink(linkName, linkUrl) {
    return new Promise((resolve, reject) => {
        const sql = 'SELECT * FROM friend_links WHERE linkName = ? AND linkUrl = ?';
        db.query(sql, [linkName, linkUrl], (error, results) => {
            if (error) {
                reject(error);
            } else {
                resolve(results.length > 0 ? results[0] : null);
            }
        });
    });
}

// 添加友链
exports.addLinks = async (req, res) => {
    try {
        // 从请求体中获取友链信息
        const { cateGroup, isActived, linkName, linkUrl, className, linkLogo, sort ,linkDesc} = req.body;

        // 检查是否已存在相同的友链信息
        const existingLink = await checkExistingLink(linkName, linkUrl);

        // 如果已存在相同的友链，则返回错误信息
        if (existingLink) {
            return res.status(400).send({
                code: 400,
                message: '该友链已存在'
            });
        }

        // 构建要插入的数据对象
        const linkData = {
            cateGroup,
            isActived,
            linkName,
            linkUrl,
            className,
            linkLogo,
            sort,
            linkDesc
        };

        // 插入到数据库中
        const sql = 'INSERT INTO friend_links SET ?';
        db.query(sql, linkData, (error, result) => {
            if (error) {
                res.status(500).send({
                    code: 500,
                    message: '插入数据失败'
                });
            } else {
                // 返回成功插入的响应
                res.status(200).send({
                    code: 200,
                    message: '插入数据成功',
                    data: result
                });
            }
        });
    } catch (error) {
        // 如果插入过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '插入数据失败'
        });
    }
};


// 删除对应linkId的友链
exports.deleteLinks = async (req, res) => {
    try {
        // 从请求参数中获取要删除的 linkId 数组
        const { linkIds } = req.body;
        // console.log('body',linkIds,typeof linkIds)

        // 构建 SQL 查询语句
        const sql = 'DELETE FROM friend_links WHERE linkId IN (?)';

        // 执行 SQL 查询并传入 linkIds 数组
        db.query(sql, [linkIds], (error, result) => {
            if (error) {
                console.error('删除友链失败:', error);
                // 发送删除失败的响应
                res.status(500).send({
                    code: 500,
                    message: '删除友链失败'
                });
            } else {
                // 检查是否有受影响的行数，如果有表示删除成功
                if (result.affectedRows > 0) {
                    // 发送删除成功的响应
                    res.status(200).send({
                        code: 200,
                        message: '删除友链成功'
                    });
                } else {
                    // 如果没有受影响的行数，表示没有找到要删除的友链，发送友链不存在的响应
                    res.status(404).send({
                        code: 404,
                        message: '找不到要删除的友链'
                    });
                }
            }
        });
    } catch (error) {
        console.error('删除友链失败:', error);
        // 发送删除失败的响应
        res.status(500).send({
            code: 500,
            message: '删除友链失败'
        });
    }
};


// // 查询所有友链并按照cateGroup字段分组
// exports.queryLinks = async (req, res) => {
//     try {
//         let {authorization} = req.headers
//         let isAdmin = false
//         isAdmin =  authorization ? verifyToken(authorization.split(" ")[1]) ? true : false : false
//         // 查询数据库获取所有友链信息并按照cateGroup字段分组
//         const sql = 'SELECT cateGroup, JSON_ARRAYAGG(JSON_OBJECT("linkId",linkId,"linkName", linkName, "linkUrl", linkUrl,"linkLogo",linkLogo,"isActived",isActived,"createTime",createTime,"linkDesc",linkDesc)) AS links, COUNT(*) AS totalLinks FROM friend_links GROUP BY cateGroup';
//         db.query(sql, (error, results) => {
//             if (error) {
//                 // 如果查询过程中发生错误，返回错误信息
//                 res.status(500).send({
//                     code: 500,
//                     message: '查询友链失败'
//                 });
//             } else {
//                 const parsedResults = results.map(result => {
//                     let links = JSON.parse(result.links)
//                     return ({
//                         cateGroup: result.cateGroup,
//                         links, // 将字符串转换为JSON对象
//                         totalLinks: result.totalLinks
//                     })
//                 });
//                 parsedResults.forEach(v => {
//                     v.links.forEach((link,index) => {
//                         // 如果当前是未激活然后还不是后台页面则不显示
//                         if(link.isActived == 0 && !isAdmin) {
//                             v.links.splice(index, 1)
//                         }
//                     })
//                 })
//                 // 返回查询到的所有友链信息按照cateGroup字段分组
//                 res.status(200).send({
//                     code: 200,
//                     message: '查询友链成功',
//                     data: parsedResults
//                 });
//             }
//         });
//
//
//     } catch (error) {
//         console.log(error)
//         // 如果查询过程中发生错误，返回错误信息
//         res.status(500).send({
//             code: 500,
//             message: '查询友链失败'
//         });
//     }
// };


// 查询所有友链并按照cateGroup字段分组
exports.queryLinks = async (req, res) => {
    try {
        let { authorization } = req.headers;
        let isAdmin = false;
        isAdmin = authorization ? verifyToken(authorization.split(" ")[1]) ? true : false : false;

        // 查询数据库获取所有友链信息并按照cateGroup字段分组的SQL语句
        const sql = `SELECT g.groupName AS groupName,
                            f.cateGroup AS groupId,
                            JSON_ARRAYAGG(JSON_OBJECT(
                                "linkId", f.linkId,
                                "linkName", f.linkName,
                                "linkUrl", f.linkUrl,
                                "linkLogo", f.linkLogo,
                                "isActived", f.isActived,
                                "createTime", f.createTime,
                                "updateTime",f.updateTime,
                                "linkDesc", f.linkDesc
                            )) AS links,
                            COUNT(*) AS totalLinks 
                     FROM friend_links f
                     JOIN group_links g ON f.cateGroup = g.groupId
                     GROUP BY f.cateGroup, g.groupName`;

        // 执行查询
        db.query(sql, (error, results) => {
            if (error) {
                // 如果查询过程中发生错误，返回错误信息
                res.status(500).send({
                    code: 500,
                    message: '查询友链失败'
                });
            } else {
                const parsedResults = results.map(result => {
                    let links = JSON.parse(result.links);
                    return ({
                        cateGroup: result.groupId,
                        groupName: result.groupName,
                        links, // 将字符串转换为JSON对象
                        totalLinks: result.totalLinks
                    });
                });

                // 如果当前是未激活然后还不是后台页面则不显示
                parsedResults.forEach(v => {
                    v.links = v.links.filter(link => isAdmin || link.isActived != 0);
                });

                // 返回查询到的所有友链信息按照cateGroup字段分组
                res.status(200).send({
                    code: 200,
                    message: '查询友链成功',
                    data: parsedResults
                });
            }
        });
    } catch (error) {
        console.log(error);
        // 如果查询过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '查询友链失败'
        });
    }
};



// 更新友链信息
// 更新友链信息
exports.updateLink = async (req, res) => {
    try {
        // 从请求体中获取前端传递的字段和值
        let { linkId, updateFields } = req.body;
        if(typeof  updateFields == 'string') {
            updateFields = JSON.parse(updateFields)
        }
        // 构建动态的 SQL 更新语句
        let sql = 'UPDATE friend_links SET';
        const values = [];
        let index = 0;
        Object.entries(updateFields).forEach(([fieldName, fieldValue]) => {
            sql += ` ${fieldName} = ?`;
            values.push(fieldValue);
            index++;
            // 如果不是最后一个字段，则添加逗号分隔符
            if (index < Object.keys(updateFields).length) {
                sql += ',';
            }
        });
        sql += ' WHERE linkId = ?';
        values.push(linkId);

        // 执行 SQL 更新语句
        db.query(sql, values, (error, result) => {
            if (error) {
                // 如果更新过程中发生错误，返回错误信息
                res.status(500).send({
                    code: 500,
                    message: '更新友链信息失败'
                });
            } else {
                // 返回成功更新的响应
                res.status(200).send({
                    code: 200,
                    message: '更新友链信息成功',
                    data: result
                });
            }
        });
    } catch (error) {
        console.log(error)
        // 如果更新过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '更新友链信息失败'
        });
    }
};



