const multer = require('multer')
const fs = require('fs')
const path = require('path')
let uploadFolder = multer({dest:'upload/'}) // 设置上传的文件夹
exports.upload = uploadFolder.single('file')


// 定义上传处理函数
exports.uploadHandler = (req,res) => {
    try {
        // 获取文件信息
        let {mimetype,filename,path,originalname} = req.file
        // 设置文件名
        let _fileName = path + '.' + mimetype.split('/')[1]
        console.log(_fileName)
        // 重命名文件
        fs.renameSync(path,`${_fileName}`)
        // 返回文件信息
        res.send({
            code:200,
            message:'图片上传成功',
            data:{
                url:_fileName,
                originalname,
            }
        })
    }catch (e) {
        // 返回错误信息
        res.send({code:400,message:e.message})
    }
}
