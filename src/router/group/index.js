const db = require('../../db/index');


// 检查是否已存在相同的友链分组信息
async function checkExistingGroup(groupName) {
    return new Promise((resolve, reject) => {
        const sql = 'SELECT * FROM group_links WHERE groupName = ?';
        db.query(sql, groupName, (error, results) => {
            if (error) {
                reject(error);
            } else {
                // 如果存在相同的友链分组，则返回 true，否则返回 false
                resolve(results.length > 0);
            }
        });
    });
}



// 添加友链分组
exports.addLinkGroup = async (req, res) => {
    try {
        // 从请求体中获取友链分组信息
        const { groupName, groupDesc, isActived } = req.body;

        // 检查是否已存在相同的友链分组信息
        const existingGroup = await checkExistingGroup(groupName);

        // 如果已存在相同的友链分组，则返回错误信息
        if (existingGroup) {
            return res.status(400).send({
                code: 400,
                message: '该友链分组已存在'
            });
        }

        // 构建要插入的数据对象
        const groupData = {
            groupName,
            groupDesc,
            isActived: isActived || 1, // 默认启用
        };

        // 插入到数据库中
        const sql = 'INSERT INTO group_links SET ?';
        db.query(sql, groupData, (error, result) => {
            if (error) {
                res.status(500).send({
                    code: 500,
                    message: '插入数据失败'
                });
            } else {
                // 返回成功插入的响应
                res.status(200).send({
                    code: 200,
                    message: '插入数据成功',
                    data: result
                });
            }
        });
    } catch (error) {
        // 如果插入过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '插入数据失败'
        });
    }
};


// 动态更新友链分组信息
exports.updateDynamicLinkGroup = async (req, res) => {
    try {
        // 从请求体中获取友链分组信息
        let { groupId } = req.params; // 获取要更新的分组ID
        groupId = groupId * 1
        const updateFields = req.body; // 获取要更新的字段和值

        console.log(updateFields)

        let sql = 'UPDATE group_links SET';
        const values = [];
        let index = 0;
        Object.entries(updateFields).forEach(([fieldName, fieldValue]) => {
            sql += ` ${fieldName} = ?`;
            values.push(fieldValue);
            index++;
            // 如果不是最后一个字段，则添加逗号分隔符
            if (index < Object.keys(updateFields).length) {
                sql += ',';
            }
        });
        sql += ' WHERE groupId = ?';
        values.push(groupId);

        db.query(sql, values, (error, result) => {
            if (error) {
                res.status(500).send({
                    code: 500,
                    message: '更新数据失败'
                });
            } else {
                // 返回成功更新的响应
                res.status(200).send({
                    code: 200,
                    message: '更新数据成功',
                    data: result
                });
            }
        });
    } catch (error) {
        // 如果更新过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '更新数据失败'
        });
    }
};

// exports.deleteGroupLinks = (req,res) => {
//     let {groupId} = req.params
// }

// 删除友链分组及其下的友链
exports.deleteGroupLinks = async (req, res) => {
    try {
        // 从请求参数中获取要删除的分组ID
        const { groupId } = req.params;

        // 删除分组下的友链
        const deleteFriendLinksSQL = 'DELETE FROM friend_links WHERE cateGroup = ?';
        db.query(deleteFriendLinksSQL, [groupId], function(error1, result1) {
            if (error1) {
                return res.status(500).send({
                    code: 500,
                    message: '删除友链失败'
                });
            }

            // 删除分组
            const deleteGroupSQL = 'DELETE FROM group_links WHERE groupId = ?';
            db.query(deleteGroupSQL, [groupId], function(error2, result2) {
                if (error2) {
                    return res.status(500).send({
                        code: 500,
                        message: '删除分组失败'
                    });
                }

                // 返回成功删除的响应
                res.status(200).send({
                    code: 200,
                    message: '删除成功'
                });
            });
        });
    } catch (error) {
        // 如果删除过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '删除数据失败'
        });
    }
};


// 查询所有的分组
// 查询全部分组信息
exports.getAllGroups = async (req, res) => {
    try {
        // 执行查询全部分组的 SQL 查询语句
        const sql = 'SELECT * FROM group_links';

        // 执行查询
        db.query(sql, (error, results) => {
            if (error) {
                // 如果查询出错，返回错误信息
                res.status(500).send({
                    code: 500,
                    message: '查询分组信息失败'
                });
            } else {
                // 如果查询成功，返回查询结果
                res.status(200).send({
                    code: 200,
                    message: '查询分组信息成功',
                    data: results
                });
            }
        });
    } catch (error) {
        // 如果查询过程中发生错误，返回错误信息
        res.status(500).send({
            code: 500,
            message: '查询分组信息失败'
        });
    }
};


