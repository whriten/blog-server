-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2023-12-11 17:28:37
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `blog_sql`
--

-- --------------------------------------------------------

--
-- 表的结构 `articles`
--

CREATE TABLE `articles` (
  `articleId` int(11) NOT NULL COMMENT '文章的id',
  `articleTitle` varchar(100) NOT NULL DEFAULT '未命名文章' COMMENT '文章标题',
  `articleContent` text NOT NULL COMMENT '文章内容',
  `publicTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '文章发表时间',
  `publicUserId` varchar(100) NOT NULL COMMENT '发表用户的id',
  `likeCount` varchar(100) NOT NULL DEFAULT '0' COMMENT '点赞数',
  `repliesCount` varchar(100) NOT NULL DEFAULT '0' COMMENT '回复数量',
  `viewsCount` varchar(100) NOT NULL DEFAULT '0' COMMENT '浏览数',
  `cates` varchar(100) NOT NULL COMMENT '分类',
  `tags` varchar(100) NOT NULL COMMENT '标签',
  `articleCover` varchar(256) NOT NULL DEFAULT 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png' COMMENT '文章的封面字段',
  `articleDesc` varchar(100) NOT NULL COMMENT '文章的大致意思简介'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `articles`
--

INSERT INTO `articles` (`articleId`, `articleTitle`, `articleContent`, `publicTime`, `publicUserId`, `likeCount`, `repliesCount`, `viewsCount`, `cates`, `tags`, `articleCover`, `articleDesc`) VALUES
(10, '测试456题', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', '测试的撒点哈手机壳沙河大街卡仕达酱卡仕达酱哈萨克大家啥活动空间啊啥的看见好久'),
(2, '测试文章标题', '测试文章内容', '2023-12-07 10:53:47', '8', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', '是啊打扫打扫打扫的的是啊打扫打阿萨德'),
(3, '测试文章标题', '测试文章内容', '2023-12-07 10:53:47', '8', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', '啊是哒是哒是哒实打实的爱是的'),
(8, '测试456题', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', '是啊打扫打扫打扫打扫的'),
(9, '测试456题', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', '啊是哒是哒是哒是哒是的'),
(6, '测试文章标题', '测试文章内容', '2023-11-29 14:58:39', '8', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', ''),
(7, '测试文章标题', '测试文章内容', '2023-12-07 10:53:47', '8', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://img.zcool.cn/community/01763059ec3f64a801202b0ce6a25c.jpg@1280w_1l_2o_100sh.jpg', '啊是哒是哒是哒的'),
(12, '测试456题', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '四十多岁的'),
(13, '测试456题', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '231232132321sdfsfsdfdsfdsf'),
(14, '测试自己带文章封面', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', 'sdfsfsdfsdfsdfsdf'),
(15, '测试自己带文章封面', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(16, '测试自己带文章封面', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(17, '占山', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(18, '占山', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(19, '占山', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(20, '占山', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(21, '占山', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(22, '占山', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(23, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(24, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(25, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(26, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(27, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(28, '12313', '122323231321354564654656546465', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(29, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'http\r\ns://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(30, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'http\r\ns://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(31, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'http\r\ns://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(32, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(33, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(34, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(35, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(36, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(37, '12313', '文章的内容创作', '2023-12-07 10:53:47', '10', '0', '0', '0', '1,2,3,4', '1,2,3', 'http://baidu.com', '是啊打扫打扫打扫打扫的'),
(38, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'http\r\ns://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(39, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'http\r\ns://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(40, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.c\r\nom/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(41, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(42, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(43, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(44, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(45, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(46, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(47, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '是啊打扫打扫打扫打扫的'),
(48, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '是啊打扫打扫打扫打扫的'),
(49, '测4545645', 'zahnsah文章', '2023-12-11 01:32:14', '9', '0', '0', '4545', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '是啊打扫打扫打扫打扫的'),
(50, '测4545645', 'zahnsah文章', '2023-12-11 01:32:22', '9', '456456', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '是啊打扫打扫打扫打扫的'),
(51, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '是啊打扫打扫打扫打扫的'),
(52, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '是啊打扫打扫打扫打扫的'),
(53, '测4545645', 'zahnsah文章', '2023-12-07 10:53:47', '9', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://notes.hurleywong.com/assets/static/yuwan-code-analysis.a209973.9cf9019424e18a0b33493a66f6337510.png', '是啊打扫打扫打扫打扫的'),
(54, 'zhansgadhjsadasdasdd', '#### 床大金价廉学则起\n:::center\n\n:::', '2023-12-11 01:32:22', '8', '0', '456', '45546', '1,2,3', '1,2,3', 'https://tse2-mm.cn.bing.net/th/id/OIP-C.JuSM25g1b2eEYu-2KbGpVwHaHI?rs=1&pid=ImgDetMain', '是啊打扫打扫打扫打扫的'),
(55, '创建第一篇文章是的很不错', '#### chuangjian\n上传当前文章封面哈哈哈哈给当前世界杯', '2023-12-07 10:53:47', '8', '0', '0', '0', '1,2,3', '1,2,3', 'https://tse2-mm.cn.bing.net/th/id/OIP-C.JuSM25g1b2eEYu-2KbGpVwHaHI?rs=1&pid=ImgDetMain', '是啊打扫打扫打扫打扫的'),
(56, '第一篇测试文章', '# Welcome to Leanote! 欢迎来到Leanote!\n \n## 1. 排版\n \n**粗体** *斜体* \n \n~~这是一段错误的文本。~~\n \n引用:\n \n> 引用Leanote官方的话, 为什么要做Leanote, 原因是...\n \n有充列表:\n 1. 支持Vim\n 2. 支持Emacs\n \n无序列表:\n \n - 项目1\n - 项目2\n \n \n## 2. 图片与链接\n \n图片:\n![leanote](http://leanote.com/images/logo/leanote_icon_blue.png)\n链接:\n \n[这是去往Leanote官方博客的链接](http://leanote.leanote.com)\n \n## 3. 标题\n \n以下是各级标题, 最多支持5级标题\n \n```\n# h1\n## h2\n### h3\n#### h4\n##### h4\n###### h5\n```\n \n## 4. 代码\n \n示例:\n \n    function get(key) {\n        return m[key];\n    }\n    \n代码高亮示例:\n \n``` javascript\n/**\n* nth element in the fibonacci series.\n* @param n >= 0\n* @return the nth element, >= 0.\n*/\nfunction fib(n) {\n  var a = 1, b = 1;\n  var tmp;\n  while (--n >= 0) {\n    tmp = a;\n    a += b;\n    b = tmp;\n  }\n  return a;\n}\n \ndocument.write(fib(10));\n```\n \n```python\nclass Employee:\n   empCount = 0\n \n   def __init__(self, name, salary):\n        self.name = name\n        self.salary = salary\n        Employee.empCount += 1\n```\n \n# 5. Markdown 扩展\n \nMarkdown 扩展支持:\n \n* 表格\n* 定义型列表\n* Html 标签\n* 脚注\n* 目录\n* 时序图与流程图\n* MathJax 公式\n \n## 5.1 表格\n \nItem     | Value\n-------- | ---\nComputer | $1600\nPhone    | $12\nPipe     | $1\n \n可以指定对齐方式, 如Item列左对齐, Value列右对齐, Qty列居中对齐\n \n| Item     | Value | Qty   |\n| :------- | ----: | :---: |\n| Computer | $1600 |  5    |\n| Phone    | $12   |  12   |\n| Pipe     | $1    |  234  |\n \n \n## 5.2 定义型列表\n \n名词 1\n:   定义 1（左侧有一个可见的冒号和四个不可见的空格）\n \n代码块 2\n:   这是代码块的定义（左侧有一个可见的冒号和四个不可见的空格）\n \n        代码块（左侧有八个不可见的空格）\n \n## 5.3 Html 标签\n \n支持在 Markdown 语法中嵌套 Html 标签，譬如，你可以用 Html 写一个纵跨两行的表格：\n \n    <table>\n        <tr>\n            <th rowspan=\"2\">值班人员</th>\n            <th>星期一</th>\n            <th>星期二</th>\n            <th>星期三</th>\n        </tr>\n        <tr>\n            <td>李强</td>\n            <td>张明</td>\n            <td>王平</td>\n        </tr>\n    </table>\n \n \n<table>\n    <tr>\n        <th rowspan=\"2\">值班人员</th>\n        <th>星期一</th>\n        <th>星期二</th>\n        <th>星期三</th>\n    </tr>\n    <tr>\n        <td>李强</td>\n        <td>张明</td>\n        <td>王平</td>\n    </tr>\n</table>\n \n**提示**, 如果想对图片的宽度和高度进行控制, 你也可以通过img标签, 如:\n \n<img src=\"http://leanote.com/images/logo/leanote_icon_blue.png\" width=\"50px\" />\n \n## 5.4 脚注\n \nLeanote[^footnote]来创建一个脚注\n  [^footnote]: Leanote是一款强大的开源云笔记产品.\n \n## 5.5 目录\n \n通过 `[TOC]` 在文档中插入目录, 如:\n \n[TOC]\n \n## 5.6 时序图与流程图\n \n```sequence\nAlice->Bob: Hello Bob, how are you?\nNote right of Bob: Bob thinks\nBob-->Alice: I am good thanks!\n```\n \n流程图:\n \n```flow\nst=>start: Start\ne=>end\nop=>operation: My Operation\ncond=>condition: Yes or No?\n \nst->op->cond\ncond(yes)->e\ncond(no)->op\n```\n \n> **提示:** 更多关于时序图与流程图的语法请参考:\n \n> - [时序图语法](http://bramp.github.io/js-sequence-diagrams/)\n> - [流程图语法](http://adrai.github.io/flowchart.js)\n \n## 5.7 MathJax 公式\n \n访问 [MathJax](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference) 参考更多使用方法。\n\n#### 测试发表\n![2296241252.jfif](http://localhost:6066/upload/b757cd7c6a189d9b90360b84f0921165.jpeg){{{width=\"50%\" height=\"auto\"}}}\n\n### 文件上传当前\n\n```\n测试文章列表\n```', '2023-12-07 10:53:47', '8', '0', '0', '0', '11', '1,3,2', 'upload2d42b20b4b4a8d4ab41c411dd9042e94.jpeg', '是啊打扫打扫打扫打扫的'),
(57, '第一篇测试文章', '# Welcome to Leanote! 欢迎来到Leanote!\n \n## 1. 排版\n \n**粗体** *斜体* \n \n~~这是一段错误的文本。~~\n \n引用:\n \n> 引用Leanote官方的话, 为什么要做Leanote, 原因是...\n \n有充列表:\n 1. 支持Vim\n 2. 支持Emacs\n \n无序列表:\n \n - 项目1\n - 项目2\n \n \n## 2. 图片与链接\n \n图片:\n![leanote](http://leanote.com/images/logo/leanote_icon_blue.png)\n链接:\n \n[这是去往Leanote官方博客的链接](http://leanote.leanote.com)\n \n## 3. 标题\n \n以下是各级标题, 最多支持5级标题\n \n```\n# h1\n## h2\n### h3\n#### h4\n##### h4\n###### h5\n```\n \n## 4. 代码\n \n示例:\n \n    function get(key) {\n        return m[key];\n    }\n    \n代码高亮示例:\n \n``` javascript\n/**\n* nth element in the fibonacci series.\n* @param n >= 0\n* @return the nth element, >= 0.\n*/\nfunction fib(n) {\n  var a = 1, b = 1;\n  var tmp;\n  while (--n >= 0) {\n    tmp = a;\n    a += b;\n    b = tmp;\n  }\n  return a;\n}\n \ndocument.write(fib(10));\n```\n \n```python\nclass Employee:\n   empCount = 0\n \n   def __init__(self, name, salary):\n        self.name = name\n        self.salary = salary\n        Employee.empCount += 1\n```\n \n# 5. Markdown 扩展\n \nMarkdown 扩展支持:\n \n* 表格\n* 定义型列表\n* Html 标签\n* 脚注\n* 目录\n* 时序图与流程图\n* MathJax 公式\n \n## 5.1 表格\n \nItem     | Value\n-------- | ---\nComputer | $1600\nPhone    | $12\nPipe     | $1\n \n可以指定对齐方式, 如Item列左对齐, Value列右对齐, Qty列居中对齐\n \n| Item     | Value | Qty   |\n| :------- | ----: | :---: |\n| Computer | $1600 |  5    |\n| Phone    | $12   |  12   |\n| Pipe     | $1    |  234  |\n \n \n## 5.2 定义型列表\n \n名词 1\n:   定义 1（左侧有一个可见的冒号和四个不可见的空格）\n \n代码块 2\n:   这是代码块的定义（左侧有一个可见的冒号和四个不可见的空格）\n \n        代码块（左侧有八个不可见的空格）\n \n## 5.3 Html 标签\n \n支持在 Markdown 语法中嵌套 Html 标签，譬如，你可以用 Html 写一个纵跨两行的表格：\n \n    <table>\n        <tr>\n            <th rowspan=\"2\">值班人员</th>\n            <th>星期一</th>\n            <th>星期二</th>\n            <th>星期三</th>\n        </tr>\n        <tr>\n            <td>李强</td>\n            <td>张明</td>\n            <td>王平</td>\n        </tr>\n    </table>\n \n \n<table>\n    <tr>\n        <th rowspan=\"2\">值班人员</th>\n        <th>星期一</th>\n        <th>星期二</th>\n        <th>星期三</th>\n    </tr>\n    <tr>\n        <td>李强</td>\n        <td>张明</td>\n        <td>王平</td>\n    </tr>\n</table>\n \n**提示**, 如果想对图片的宽度和高度进行控制, 你也可以通过img标签, 如:\n \n<img src=\"http://leanote.com/images/logo/leanote_icon_blue.png\" width=\"50px\" />\n \n## 5.4 脚注\n \nLeanote[^footnote]来创建一个脚注\n  [^footnote]: Leanote是一款强大的开源云笔记产品.\n \n## 5.5 目录\n \n通过 `[TOC]` 在文档中插入目录, 如:\n \n[TOC]\n \n## 5.6 时序图与流程图\n \n```sequence\nAlice->Bob: Hello Bob, how are you?\nNote right of Bob: Bob thinks\nBob-->Alice: I am good thanks!\n```\n \n流程图:\n \n```flow\nst=>start: Start\ne=>end\nop=>operation: My Operation\ncond=>condition: Yes or No?\n \nst->op->cond\ncond(yes)->e\ncond(no)->op\n```\n \n> **提示:** 更多关于时序图与流程图的语法请参考:\n \n> - [时序图语法](http://bramp.github.io/js-sequence-diagrams/)\n> - [流程图语法](http://adrai.github.io/flowchart.js)\n \n## 5.7 MathJax 公式\n \n访问 [MathJax](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference) 参考更多使用方法。\n\n#### 测试发表\n![2296241252.jfif](http://localhost:6066/upload/b757cd7c6a189d9b90360b84f0921165.jpeg){{{width=\"50%\" height=\"auto\"}}}\n\n### 文件上传当前\n\n```\n测试文章列表\n```', '2023-12-07 10:53:47', '8', '0', '0', '0', '11', '1,3,2', 'upload/50f94334a34a63bd50feaf189a7caa7d.jpeg', '是啊打扫打扫打扫打扫的'),
(58, '时序图.shoperli创建当前页面', '# Welcome to Leanote! 欢迎来到Leanote!\n \n## 1. 排版\n \n**粗体** *斜体* \n \n~~这是一段错误的文本。~~\n \n引用:\n \n> 引用Leanote官方的话, 为什么要做Leanote, 原因是...\n \n有充列表:\n 1. 支持Vim\n 2. 支持Emacs\n \n无序列表:\n \n - 项目1\n - 项目2\n \n \n## 2. 图片与链接\n \n图片:\n![leanote](http://leanote.com/images/logo/leanote_icon_blue.png)\n链接:\n \n[这是去往Leanote官方博客的链接](http://leanote.leanote.com)\n \n## 3. 标题\n \n以下是各级标题, 最多支持5级标题\n \n```\n# h1\n## h2\n### h3\n#### h4\n##### h4\n###### h5\n```\n \n## 4. 代码\n \n示例:\n \n    function get(key) {\n        return m[key];\n    }\n    \n代码高亮示例:\n \n``` javascript\n/**\n* nth element in the fibonacci series.\n* @param n >= 0\n* @return the nth element, >= 0.\n*/\nfunction fib(n) {\n  var a = 1, b = 1;\n  var tmp;\n  while (--n >= 0) {\n    tmp = a;\n    a += b;\n    b = tmp;\n  }\n  return a;\n}\n \ndocument.write(fib(10));\n```\n \n```python\nclass Employee:\n   empCount = 0\n \n   def __init__(self, name, salary):\n        self.name = name\n        self.salary = salary\n        Employee.empCount += 1\n```\n \n# 5. Markdown 扩展\n \nMarkdown 扩展支持:\n \n* 表格\n* 定义型列表\n* Html 标签\n* 脚注\n* 目录\n* 时序图与流程图\n* MathJax 公式\n \n## 5.1 表格\n \nItem     | Value\n-------- | ---\nComputer | $1600\nPhone    | $12\nPipe     | $1\n \n可以指定对齐方式, 如Item列左对齐, Value列右对齐, Qty列居中对齐\n \n| Item     | Value | Qty   |\n| :------- | ----: | :---: |\n| Computer | $1600 |  5    |\n| Phone    | $12   |  12   |\n| Pipe     | $1    |  234  |\n \n \n## 5.2 定义型列表\n \n名词 1\n:   定义 1（左侧有一个可见的冒号和四个不可见的空格）\n \n代码块 2\n:   这是代码块的定义（左侧有一个可见的冒号和四个不可见的空格）\n \n        代码块（左侧有八个不可见的空格）\n \n## 5.3 Html 标签\n \n支持在 Markdown 语法中嵌套 Html 标签，譬如，你可以用 Html 写一个纵跨两行的表格：\n \n    <table>\n        <tr>\n            <th rowspan=\"2\">值班人员</th>\n            <th>星期一</th>\n            <th>星期二</th>\n            <th>星期三</th>\n        </tr>\n        <tr>\n            <td>李强</td>\n            <td>张明</td>\n            <td>王平</td>\n        </tr>\n    </table>\n \n \n<table>\n    <tr>\n        <th rowspan=\"2\">值班人员</th>\n        <th>星期一</th>\n        <th>星期二</th>\n        <th>星期三</th>\n    </tr>\n    <tr>\n        <td>李强</td>\n        <td>张明</td>\n        <td>王平</td>\n    </tr>\n</table>\n \n**提示**, 如果想对图片的宽度和高度进行控制, 你也可以通过img标签, 如:\n \n<img src=\"http://leanote.com/images/logo/leanote_icon_blue.png\" width=\"50px\" />\n \n## 5.4 脚注\n \nLeanote[^footnote]来创建一个脚注\n  [^footnote]: Leanote是一款强大的开源云笔记产品.\n \n## 5.5 目录\n \n通过 `[TOC]` 在文档中插入目录, 如:\n \n[TOC]\n \n## 5.6 时序图与流程图\n \n```sequence\nAlice->Bob: Hello Bob, how are you?\nNote right of Bob: Bob thinks\nBob-->Alice: I am good thanks!\n```\n \n流程图:\n \n```flow\nst=>start: Start\ne=>end\nop=>operation: My Operation\ncond=>condition: Yes or No?\n \nst->op->cond\ncond(yes)->e\ncond(no)->op\n```\n \n> **提示:** 更多关于时序图与流程图的语法请参考:\n \n> - [时序图语法](http://bramp.github.io/js-sequence-diagrams/)\n> - [流程图语法](http://adrai.github.io/flowchart.js)\n \n## 5.7 MathJax 公式\n \n$ 表示行内公式： \n \n质能守恒方程可以用一个很简洁的方程式 $E=mc^2$ 来表达。\n \n$$ 表示整行公式：\n \n$$sum_{i=1}^n a_i=0$$\n \n$$f(x_1,x_x,ldots,x_n) = x_1^2 + x_2^2 + cdots + x_n^2 $$\n \n$$sum^{j-1}_{k=0}{widehat{gamma}_{kj} z_k}$$\n \n更复杂的公式:\n$$\negin{eqnarray}\nvec\nabla 	imes (vec\nabla f) & = & 0  cdotscdots梯度场必是无旋场\\\nvec\nabla cdot(vec\nabla 	imes vec F) & = & 0cdotscdots旋度场必是无散场\\\nvec\nabla cdot (vec\nabla f) & = & {vec\nabla}^2f\\\nvec\nabla 	imes(vec\nabla 	imes vec F) & = & vec\nabla(vec\nabla cdot vec F) - {vec\nabla}^2 vec F\\\nend{eqnarray}\n$$\n \n访问 [MathJax](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference) 参考更多使用方法。', '2023-12-06 10:01:55', '8', '0', '0', '0', '1,2', '1,3,4,5,7,8', 'upload/6c7cc7907c0b3c0d034deed7556c8b87.jpeg', ''),
(59, '时序图.shoperli是', '# Welcome to Leanote! 欢迎来到Leanote!\n \n## 1. 排版\n \n**粗体** *斜体* \n \n~~这是一段错误的文本。~~\n \n引用:\n \n> 引用Leanote官方的话, 为什么要做Leanote, 原因是...\n \n有充列表:\n 1. 支持Vim\n 2. 支持Emacs\n \n无序列表:\n \n - 项目1\n - 项目2\n \n \n## 2. 图片与链接\n \n图片:\n![leanote](http://leanote.com/images/logo/leanote_icon_blue.png)\n链接:\n \n[这是去往Leanote官方博客的链接](http://leanote.leanote.com)\n \n## 3. 标题\n \n以下是各级标题, 最多支持5级标题\n \n```\n# h1\n## h2\n### h3\n#### h4\n##### h4\n###### h5\n```\n \n## 4. 代码\n \n示例:\n \n    function get(key) {\n        return m[key];\n    }\n    \n代码高亮示例:\n \n``` javascript\n/**\n* nth element in the fibonacci series.\n* @param n >= 0\n* @return the nth element, >= 0.\n*/\nfunction fib(n) {\n  var a = 1, b = 1;\n  var tmp;\n  while (--n >= 0) {\n    tmp = a;\n    a += b;\n    b = tmp;\n  }\n  return a;\n}\n \ndocument.write(fib(10));\n```\n \n```python\nclass Employee:\n   empCount = 0\n \n   def __init__(self, name, salary):\n        self.name = name\n        self.salary = salary\n        Employee.empCount += 1\n```\n \n# 5. Markdown 扩展\n \nMarkdown 扩展支持:\n \n* 表格\n* 定义型列表\n* Html 标签\n* 脚注\n* 目录\n* 时序图与流程图\n* MathJax 公式\n \n## 5.1 表格\n \nItem     | Value\n-------- | ---\nComputer | $1600\nPhone    | $12\nPipe     | $1\n \n可以指定对齐方式, 如Item列左对齐, Value列右对齐, Qty列居中对齐\n \n| Item     | Value | Qty   |\n| :------- | ----: | :---: |\n| Computer | $1600 |  5    |\n| Phone    | $12   |  12   |\n| Pipe     | $1    |  234  |\n \n \n## 5.2 定义型列表\n \n名词 1\n:   定义 1（左侧有一个可见的冒号和四个不可见的空格）\n \n代码块 2\n:   这是代码块的定义（左侧有一个可见的冒号和四个不可见的空格）\n \n        代码块（左侧有八个不可见的空格）\n \n## 5.3 Html 标签\n \n支持在 Markdown 语法中嵌套 Html 标签，譬如，你可以用 Html 写一个纵跨两行的表格：\n \n    <table>\n        <tr>\n            <th rowspan=\"2\">值班人员</th>\n            <th>星期一</th>\n            <th>星期二</th>\n            <th>星期三</th>\n        </tr>\n        <tr>\n            <td>李强</td>\n            <td>张明</td>\n            <td>王平</td>\n        </tr>\n    </table>\n \n \n<table>\n    <tr>\n        <th rowspan=\"2\">值班人员</th>\n        <th>星期一</th>\n        <th>星期二</th>\n        <th>星期三</th>\n    </tr>\n    <tr>\n        <td>李强</td>\n        <td>张明</td>\n        <td>王平</td>\n    </tr>\n</table>\n \n**提示**, 如果想对图片的宽度和高度进行控制, 你也可以通过img标签, 如:\n \n<img src=\"http://leanote.com/images/logo/leanote_icon_blue.png\" width=\"50px\" />\n \n## 5.4 脚注\n \nLeanote[^footnote]来创建一个脚注\n  [^footnote]: Leanote是一款强大的开源云笔记产品.\n \n## 5.5 目录\n \n通过 `[TOC]` 在文档中插入目录, 如:\n \n[TOC]\n \n## 5.6 时序图与流程图\n \n```sequence\nAlice->Bob: Hello Bob, how are you?\nNote right of Bob: Bob thinks\nBob-->Alice: I am good thanks!\n```\n \n流程图:\n \n```flow\nst=>start: Start\ne=>end\nop=>operation: My Operation\ncond=>condition: Yes or No?\n \nst->op->cond\ncond(yes)->e\ncond(no)->op\n```\n \n> **提示:** 更多关于时序图与流程图的语法请参考:\n \n> - [时序图语法](http://bramp.github.io/js-sequence-diagrams/)\n> - [流程图语法](http://adrai.github.io/flowchart.js)\n \n## 5.7 MathJax 公式\n \n$ 表示行内公式： \n \n质能守恒方程可以用一个很简洁的方程式 $E=mc^2$ 来表达。\n \n$$ 表示整行公式：\n \n$$sum_{i=1}^n a_i=0$$\n \n$$f(x_1,x_x,ldots,x_n) = x_1^2 + x_2^2 + cdots + x_n^2 $$\n \n$$sum^{j-1}_{k=0}{widehat{gamma}_{kj} z_k}$$\n \n更复杂的公式:\n$$\negin{eqnarray}\nvec\nabla 	imes (vec\nabla f) & = & 0  cdotscdots梯度场必是无旋场\\\nvec\nabla cdot(vec\nabla 	imes vec F) & = & 0cdotscdots旋度场必是无散场\\\nvec\nabla cdot (vec\nabla f) & = & {vec\nabla}^2f\\\nvec\nabla 	imes(vec\nabla 	imes vec F) & = & vec\nabla(vec\nabla cdot vec F) - {vec\nabla}^2 vec F\\\nend{eqnarray}\n$$\n \n访问 [MathJax](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference) 参考更多使用方法。', '2023-12-06 10:02:28', '8', '0', '0', '0', '1,5', '1,7,2', 'upload/db84da913c447996cb2650ddde0154f7.jpeg', ''),
(60, '当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨', '当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨当前啥断手断脚狂欢节哈哈哈嗲和四季度就就是啊好的客家黄酒看看回家撒谎的夸奖好看就是啊就是啊好的尽快哈看见哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒哒吨吨吨吨吨', '2023-12-11 01:32:28', '8', '123', '12', '1', '1,2', '1', 'upload/300ccdd951001a9c0e84b6dbaa552c45.jpeg', ''),
(61, '创作第一篇文章', '张三不能发布i\n### quesh \n\n##### 类似于大哥前不hi哈师大就啊是多久啊是大家都会\n\n:::center\n![t0141f88894b197d9d8.jpg](http://localhost:6066/upload/0758599b673c1c247413c93ac6ca0829.jpeg){{{width=\"50%\" height=\"auto\"}}}\n:::', '2023-12-07 07:30:43', '8', '0', '0', '0', '11', '1,3,4,5', 'upload/2b17b3df4448acf6f33ee90e17ed083f.jpeg', ''),
(62, '测4545645', 'zahnsah文章', '2023-12-07 10:51:58', '8', '0', '0', '0', '1,2,3,4,5', '1,2,3,4,5', 'https://foruda.gitee.com/avatar/1700451256438859959/9117143_whriten_1700451256.png!avatar200', '测试desc'),
(63, 'mysql统计id个数_MySQL的统计总数count(*)与count(id)或count(字段)的之间的各自效率性能对比...', '执行效果：\n\n1.  count(1) and count(*)\n\n当表的数据量大些时，对表作分析之后，使用count(1)还要比使用count(*)用时多了！\n\n从执行计划来看，count(1)和count(*)的效果是一样的。 但是在表做过分析之后，count(1)会比count(*)的用时少些(1w以内数据量)，不过差不了多少。\n\n如果count(1)是聚索引,id,那肯定是count(1)快。但是差的很小的。\n\n因为count(*),自动会优化指定到那一个字段。所以没必要去count(1)，用count(*)，sql会帮你完成优化的 因此：count(1)和count(*)基本没有差别！\n\n2. count(1) and count(字段)\n\n两者的主要区别是\n\n(1) count(1) 会统计表中的所有的记录数，包含字段为null 的记录。\n\n(2) count(字段) 会统计该字段在表中出现的次数，忽略字段为null 的情况。即不统计字段为null 的记录。\n\ncount(*) 和 count(1)和count(列名)区别\n\n执行效果上：\n\ncount(*)包括了所有的列，相当于行数，在统计结果的时候，不会忽略列值为NULL\n\ncount(1)包括了忽略所有列，用1代表代码行，在统计结果的时候，不会忽略列值为NULL\n\ncount(列名)只包括列名那一列，在统计结果的时候，会忽略列值为空(这里的空不是只空字符串或者0，而是表示null)的计数，即某个字段值为NULL时，不统计。\n\n执行效率上：\n\n列名为主键，count(列名)会比count(1)快\n\n列名不为主键，count(1)会比count(列名)快\n\n如果表多个列并且没有主键，则 count(1) 的执行效率优于 count(*)\n\n如果有主键，则 select count(主键)的执行效率是最优的\n\n如果表只有一个字段，则 select count(*)最优。', '2023-12-08 00:58:08', '8', '0', '0', '0', '1,5', '1,2,3', 'upload/c260052d81079b7439597303959fbb3a.jpeg', 'mysql统计id个数_MySQL的统计总数count(*)与count(id)或count(字段)的之间的各自效率性能对比...'),
(64, 'Ant Design Vue 树形表格 defaultExpandAllRows 默认展开无效', '\n## table defaultExpandAllRows 默认展开所有行\n\n### defaultExpandAllRows=true 不展开问题，因为初始数据是空的需要判断数据是否存在 v-if=\"tableData\"\n\n```html\n<a-table\n    size=\"middle\"\n	v-if=\"tableData\"\n    :columns=\"columns\"\n	:pagination=\"false\"\n	:loading=\"tableLoading\"\n	:data-source=\"tableData\"\n    :defaultExpandAllRows=\"true\"\n	@change=\"handleTableChange\">\n</a-tabe>\n```', '2023-12-08 08:41:40', '8', '0', '0', '0', '11', '2', 'upload/a2df0f38919137d90c8bdf042c12a8ff.jpeg', 'Ant Design Vue 树形表格 defaultExpandAllRows 默认展开无效'),
(65, '###是是是是是', '是啊大叔大婶打的测试tags', '2023-12-09 07:15:11', '8', '0', '0', '0', '11,23', '20', '/upload/default_cover.jpg', '是是是是是');

-- --------------------------------------------------------

--
-- 表的结构 `cates`
--

CREATE TABLE `cates` (
  `cateId` int(11) NOT NULL COMMENT '分类id',
  `cateName` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `cateAlias` varchar(100) NOT NULL COMMENT '分类别名(默认同分类名)',
  `cateDesc` varchar(100) NOT NULL COMMENT '分类描述',
  `cateParentId` int(11) NOT NULL DEFAULT '0' COMMENT '父级分类ID集合（0为顶级id节点）',
  `cateArticles` varchar(100) NOT NULL COMMENT '当前分类下面所有的文章集合'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='分类表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `cates`
--

INSERT INTO `cates` (`cateId`, `cateName`, `cateAlias`, `cateDesc`, `cateParentId`, `cateArticles`) VALUES
(1, '前端', '前端', '概括前端中所有的问题', 0, '550,50,150,150,47,48,49,50,51,52,53,54,55,58,59,60,62,63'),
(2, 'Vue', 'Vue', 'vue', 1, '10,11,100,30,47,48,49,50,51,52,53,54,55,58,60,62'),
(3, 'webPack', 'webPack', 'webPack', 1, '6,8,9,100,30,47,48,49,50,51,52,53,54,55,62'),
(4, 'HTML/CSS', 'HTML/CSS', 'HTML/CSS', 1, '7,8,6,30,47,48,49,50,51,52,53,62'),
(5, 'uniApp', 'uniApp', 'uniApp', 1, '2,10,100,27,28,30,32,47,48,49,50,51,52,53,59,62,63'),
(11, '微博', 'weibo', '微博全明星之夜', 0, '2,3,13,14,31,56,57,61,64,65'),
(13, '测试分类', '测试分类别名', '测试分类的简介信息', 11, ''),
(23, '测试分类231', '测试分类别名', '测试分类的简介信息', 11, '65');

-- --------------------------------------------------------

--
-- 表的结构 `tags`
--

CREATE TABLE `tags` (
  `tagId` int(11) NOT NULL COMMENT '标签ID',
  `tagName` varchar(100) NOT NULL COMMENT '标签名称',
  `tagAlias` varchar(100) NOT NULL COMMENT '标签别名',
  `tagDesc` varchar(100) NOT NULL COMMENT '标签描述',
  `tagArticles` text COMMENT '当前标签下面的所有的文章'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='标签表';

--
-- 转存表中的数据 `tags`
--

INSERT INTO `tags` (`tagId`, `tagName`, `tagAlias`, `tagDesc`, `tagArticles`) VALUES
(1, '前端', '前端', '前端tag', '53,54,55,56,57,58,59,60,61,62,63'),
(2, 'vue3', 'vue3', 'vue3知识点', '49,50,51,52,53,54,55,56,57,59,62,63,64'),
(3, '工具函数', '日常常用的工具函数集合', '常用工具函数集合', '49,50,51,52,53,54,55,56,57,58,61,62,63'),
(4, '后端', '后端', '后端学习', '49,50,51,52,53,58,61,62'),
(5, '算法', '算法', '算法学习', '49,50,51,52,53,58,61,62'),
(6, '爬虫', '爬虫', '爬虫学习', '150,100'),
(7, 'mysql', 'mysql', 'mysql', '58,59'),
(8, '牛客专题', '牛客专题', '牛客测试题', '58'),
(17, '123213', '123213', '12312312323', NULL),
(18, 'asdasdsa', 'asdasdsa', '12312312323', NULL),
(19, 'vue', 'vue', '12356465', NULL),
(20, 'asdsadasdasdasd', 'asdsadasdasdasd', 'asdasdasd', '65'),
(21, 'asdasd', 'asdasd', 'asdasdasd', NULL),
(23, '12', '12', 'asdasdasd', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL COMMENT '用户的id',
  `userIp` varchar(100) DEFAULT '未知IP' COMMENT '用户IP地址',
  `userName` varchar(100) NOT NULL COMMENT '用户名(用于登录因为用户不可能去拿id登录)',
  `userParty` varchar(100) NOT NULL COMMENT '用户的昵称',
  `userPassword` varchar(100) NOT NULL COMMENT '用户密码',
  `userEmail` varchar(100) DEFAULT NULL COMMENT '用户邮箱',
  `userAvatar` varchar(100) DEFAULT 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg' COMMENT '用户图像',
  `userType` int(11) NOT NULL DEFAULT '0' COMMENT '用户类型（0为普通的用户1为管理员）',
  `registerTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `userBirthday` varchar(100) DEFAULT NULL COMMENT '用户的生日',
  `userAge` varchar(100) DEFAULT NULL COMMENT '用户年龄',
  `userPhone` varchar(100) DEFAULT NULL COMMENT '用户手机号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`userId`, `userIp`, `userName`, `userParty`, `userPassword`, `userEmail`, `userAvatar`, `userType`, `registerTime`, `userBirthday`, `userAge`, `userPhone`) VALUES
(8, '未知IP', 'luox', '用户：66', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 1, '2023-11-29 09:16:03', NULL, NULL, NULL),
(9, '未知IP', '占山', '用户：87', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 1, '2023-11-30 02:30:13', NULL, NULL, NULL),
(10, '未知IP', 'lisi', '用户：92', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-11-30 02:09:21', NULL, NULL, NULL),
(11, '未知IP', '测hi傻空的和凯撒的好看', '用户：92', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:45:30', NULL, NULL, NULL),
(15, '未知IP', '与华天', '用户：70', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:45:55', NULL, NULL, NULL),
(16, '未知IP', '烽火影人', '用户：82', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:45:59', NULL, NULL, NULL),
(17, '未知IP', '改编成v', '用户：66', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:04', NULL, NULL, NULL),
(18, '未知IP', '的v嘻嘻嘻嘻的', '用户：49', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:17', NULL, NULL, NULL),
(19, '未知IP', '水水水水十分', '用户：34', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:20', NULL, NULL, NULL),
(20, '未知IP', '谔谔俄方', '用户：94', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:25', NULL, NULL, NULL),
(46, '未知IP', '小谢123', '夏博涛', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-08 03:38:13', NULL, NULL, NULL),
(22, '未知IP', '色温我的', '用户：33', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:34', NULL, NULL, NULL),
(23, '未知IP', '上下车v', '用户：25', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:38', NULL, NULL, NULL),
(24, '未知IP', '晓得伐', '用户：36', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:43', NULL, NULL, NULL),
(25, '未知IP', '爱放飞', '用户：18', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:49', NULL, NULL, NULL),
(26, '未知IP', '安抚', '用户：91', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:46:53', NULL, NULL, NULL),
(27, '未知IP', '安抚v发热', '用户：75', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:47:00', NULL, NULL, NULL),
(28, '未知IP', '23都非常', '用户：13', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-04 08:47:05', NULL, NULL, NULL),
(47, '未知IP', '小谢123456', '廖越彬', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-08 03:44:42', NULL, NULL, NULL),
(32, '未知IP', 'uius', '武懿轩', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 1, '2023-12-05 01:08:06', NULL, NULL, NULL),
(33, '未知IP', '3423', '胡雨泽', '74da09249c6bb2ee394e8bf457d899b8', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 1, '2023-12-05 01:09:59', NULL, NULL, NULL),
(42, '未知IP', 'shejifuli@qq.com', '夏明轩', '9c1d3198a7a319dc88ad3602e17c4eb9', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-05 02:36:18', NULL, NULL, NULL),
(43, '未知IP', '小谢', '沈明哲', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 1, '2023-12-05 02:36:31', NULL, NULL, NULL),
(36, '未知IP', '234234', '钱瑾瑜', '69cf8ecd3bc134748cec3ae830ab3f75', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-05 01:10:08', NULL, NULL, NULL),
(37, '未知IP', '啥的回家', '郝思远', 'b338055f9e5e7d763ed546e46784925f', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-05 01:10:13', NULL, NULL, NULL),
(45, '未知IP', '小谢er', '萧嘉熙', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-05 03:03:29', NULL, NULL, NULL),
(44, '未知IP', '技术人员', '段乐驹', '3c760cc04979f9b133de47e243db6b0c', NULL, 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41N1307687091.jpg', 0, '2023-12-05 02:53:32', NULL, NULL, NULL);

--
-- 转储表的索引
--

--
-- 表的索引 `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`articleId`) USING BTREE;

--
-- 表的索引 `cates`
--
ALTER TABLE `cates`
  ADD PRIMARY KEY (`cateId`) USING BTREE,
  ADD UNIQUE KEY `cates_un` (`cateName`);

--
-- 表的索引 `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tagId`),
  ADD UNIQUE KEY `tags_un` (`tagName`);

--
-- 表的索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`) USING BTREE,
  ADD UNIQUE KEY `users_userId_IDX` (`userId`) USING BTREE,
  ADD UNIQUE KEY `users_un` (`userName`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `articles`
--
ALTER TABLE `articles`
  MODIFY `articleId` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章的id', AUTO_INCREMENT=66;

--
-- 使用表AUTO_INCREMENT `cates`
--
ALTER TABLE `cates`
  MODIFY `cateId` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id', AUTO_INCREMENT=24;

--
-- 使用表AUTO_INCREMENT `tags`
--
ALTER TABLE `tags`
  MODIFY `tagId` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签ID', AUTO_INCREMENT=24;

--
-- 使用表AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户的id', AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
